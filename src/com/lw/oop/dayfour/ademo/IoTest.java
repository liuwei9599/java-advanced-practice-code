package com.lw.oop.dayfour.ademo;

import java.io.*;

/**
 * @author liuwei
 * @ClassName IoTest.java
 * @description: IO流 测试类
 * @date 2023年06月16日
 * @version: 1.0
 */
public class IoTest {
    public static void main(String[] args) {
        //字节输出流
        System.out.println("***************字节输出流***************");
        String url = "E:\\Ideawork\\lw_oop\\src\\com\\lw\\oop\\dayfour\\ademo\\file\\Hello.txt";
        // 使用FileOutputStream获取输出流对象
        FileOutputStream fos = null;
        try {
            File file = new File(url);
            fos = new FileOutputStream(file);
            // 写出多个字节
            for (int i = 97; i <= 122; i++) {
                fos.write(i);
            }
            // 字符串转换为字节数组
            byte[] b = "金榜题名时".getBytes();
            // 写出字节数组数据
            fos.write(b);
            // 字符串转换为字节数组
            byte[] c = "1234567890".getBytes();
            // 每次写出从2索引开始，5个字节
            fos.write(c, 2, 5);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                //关闭输出流
                fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        //字节输入流
        System.out.println("***************字节输入流***************");
        String input = "E:\\Ideawork\\lw_oop\\src\\com\\lw\\oop\\dayfour\\ademo\\file\\Hello.txt";
        FileInputStream fis = null;
        try {
            //使用File对象创建流对象
            File file1 = new File(input);
            fis = new FileInputStream(file1);
//            int b;
//            while ((b = fis.read()) != -1) {
//                System.out.print((char) b + "\t");
//            }
            System.out.println("\n**********************************");
            int len = 0;
            // 定义字节数组
            byte[] d = new byte[10];
            // 循环读取
            while ((len = fis.read(d)) != -1) {
                String s = new String(d, 0, len);
                // 读取变成字符串
                System.out.println(s);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                // 关闭流对象
                fis.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        //字符输入流
        System.out.println("****************字符输入流****************");
        String output = "E:\\Ideawork\\lw_oop\\src\\com\\lw\\oop\\dayfour\\ademo\\file\\Hello.txt";
        FileReader f = null;
        try {
            File file = new File(output);
            f = new FileReader(file);
            int len;
            char[] buf = new char[50];
            while ((len = f.read(buf)) != -1) {
                String s1 = new String(buf, 0, len);
                System.out.println(s1);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            try {
                f.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        //字符输出流
        System.out.println("****************字符输出流****************");
        String out2 = "E:\\Ideawork\\lw_oop\\src\\com\\lw\\oop\\dayfour\\ademo\\file\\test003.txt";
        FileWriter fw = null;
        try {
            File file = new File(out2);
            fw = new FileWriter(file);
            for (int i = 0; i < 10; i++) {
                fw.write("Hello00"+i+"\t");
            }

            String msg = "\n床床前明月光";
            //写入字符串
            fw.write(msg);
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            try {
                fw.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
