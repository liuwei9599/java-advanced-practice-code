package com.lw.oop.dayfour.ademo;

import java.io.File;
import java.io.IOException;

/**
 * @author liuwei
 * @ClassName FileTest.java
 * @description: File类中方法的使用
 * @date 2023年06月16日
 * @version: 1.0
 */
public class FileTest {
    public static void main(String[] args) throws IOException {
        String url="E:\\Ideawork\\lw_oop\\src\\com\\lw\\oop\\dayfour\\ademo\\file";
        File file = new File(url);
        // 判断是否是文件夹
        if (file.exists()){
            file.delete();
        }
        file.mkdir();

        String url1="E:\\Ideawork\\lw_oop\\src\\com\\lw\\oop\\dayfour\\ademo\\file\\file1";
        // 创建一个文件夹
        File fl = new File(url1);
        // 判断是否是文件夹
        if (fl.exists()){
            fl.delete();
        }
        fl.mkdir();



        File file1 = new File(file, "test.txt");
        // 判断文件是否存在
        if (file1.exists())
        {
            file1.delete();    // 存在则先删除
        }
        file1.createNewFile();     // 再创建

        //创建第二个文件
        File file2 = new File(url, "test001.txt");
        // 判断文件是否存在
        if (file1.exists())
        {
            file2.delete();    // 存在则先删除
        }
        file2.createNewFile();     // 再创建
        //创建第三个文件
        File file3 = new File("E:\\Ideawork\\lw_oop\\src\\com\\lw\\oop\\dayfour\\ademo\\file\\test002.txt");
        // 判断文件是否存在
        if (file3.exists())
        {
            file3.delete();    // 存在则先删除
        }
        file3.createNewFile();     // 再创建




    }
}
