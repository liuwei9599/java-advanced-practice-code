package com.lw.oop.dayfour.ademo;

import java.io.File;
import java.io.FileFilter;
import java.util.Date;

/**
 * @author liuwei
 * @ClassName FileOneTest.java
 * @description: TODO
 * @date 2023年06月16日
 * @version: 1.0
 */
public class FileOneTest {
    public static void main(String[] args) {
        String url = "E:\\Ideawork\\lw_oop\\src\\com\\lw\\oop\\dayfour\\ademo\\file";
        File file = new File(url);
        //文件的相关操作
        //列出url目录下的所有内容
        System.out.println("****************列出url目录下的所有内容*****************");
        File[] fileArr = file.listFiles();
        for (int i = 0; i < fileArr.length; i++) {
            System.out.println(fileArr[i].getPath());
        }
        System.out.println("文件的绝对路径：" + file.getAbsolutePath());
        System.out.println("文件的相对路径：" + file.getPath());
        System.out.println("文件名：" + file.getName());
        System.out.println("文件最后修改时间：" + new Date(file.lastModified()));
        System.out.println("文件长度：" + file.length()+ "字节");
        System.out.println("文件可读：" + file.canRead());
        System.out.println("文件可写：" + file.canWrite());
        System.out.println("文件是否存在：" + file.exists());
        System.out.println("文件是否是文件夹：" + file.isDirectory());
        System.out.println("文件是否是绝对路径：" + file.isAbsolute());
        System.out.println("文件是否是文件：" + file.isFile());
        System.out.println("文件是否是目录：" + file.isDirectory());
        System.out.println("文件所在路径："+file.getParent());

        //FileFilter：文件过滤器接口
        System.out.println("-------------FileFilter：文件过滤器接口----------");
        File[] files = file.listFiles(new FileFilter() {
            @Override
            public boolean accept(File pathname) {
                return pathname.getName().endsWith(".txt");
            }
        });
        for (File f: files) {
            System.out.println(f.getName());
        }

    }


}
