package com.lw.oop.dayfour.jdemo.studemo;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author liuwei
 * @ClassName StuTest.java
 * @description: 定义一个学生类，成员变量有姓名，年龄，性别，提供setters和getters方法以及构造方法。
 * @description: 定义一个测试类，在测试类创建多个学生对象保存到集合中，然后将集合存储到当前项目根目录下的stus.txt文件中。
 * @date 2023年06月18日
 * @version: 1.0
 */
public class StuTest {
    public static void main(String[] args) throws IOException {
        Student s1=new Student("唐僧",20,"男");
        Student s2=new Student("孙悟空",21,"男");
        Student s3=new Student("猪八戒",22,"男");
        Student s4=new Student("沙悟净",20,"男");
        List<Student> list = new ArrayList<>();
        list.add(s1);
        list.add(s2);
        list.add(s3);
        list.add(s4);
        String url="E:\\Ideawork\\lw_oop\\src\\com\\lw\\oop\\dayfour\\jdemo\\studemo\\suts.txt";
        BufferedWriter br=new BufferedWriter(new FileWriter(url));

        Iterator iterator=list.iterator();
        while (iterator.hasNext()){
            br.write(iterator.next().toString());
            br.newLine();
        }
        br.close();
    }

}
