package com.lw.oop.dayfour.jdemo;

import java.io.*;

/**
 * @author liuwei
 * @ClassName VerCode.java
 * @description: TODO
 * @date 2023年06月18日
 * @version: 1.0
 */
public class VerCode {
    /**
     * 录入验证码
     * @param
     * @throws IOException
     */
    public void inputCode() throws IOException {
        File file = new File("E:\\Ideawork\\lw_oop\\src\\com\\lw\\oop\\dayfour\\jdemo\\data.txt");
        FileWriter fi=new FileWriter(file);
        BufferedWriter bufWri=new BufferedWriter(fi);
        String str=null;
        int min = 1000;
        int max = 9999;
        for (int i = 0; i <10 ; i++) {
            int s = min + (int) (Math.random() * (max - min));
            bufWri.write(s+"\n");
        }
        bufWri.close();
        fi.close();
    }

    /**
     * 校验验证码
     * @param code
     * @throws IOException
     */
    public void verifyCode(String code) throws IOException {
        File file = new File("E:\\Ideawork\\lw_oop\\src\\com\\lw\\oop\\dayfour\\jdemo\\data.txt");
        FileReader fr=new FileReader(file);
        BufferedReader bufRea=new BufferedReader(fr);
        String[] strings = new String[100];
        String str=null;
        int i=0;
        while ((str = bufRea.readLine())!=null){
            strings[i]=str;
            i++;
        }
        for (String s : strings) {
            if (s!=null){
                if (code.equals(s)){
                    System.out.println("验证成功！");
                    break;
                }
            }else{
                System.out.println("验证失败！");
                break;
            }
        }
        bufRea.close();
        fr.close();
    }

    /**
     *现有一字符串：”我爱Java”。将该字符串保存到当前项目根目录下的text05.txt文件中。
     * @param str
     * @throws IOException
     */
    public void saveStr(String str) throws IOException {
        File file = new File("E:\\Ideawork\\lw_oop\\src\\com\\lw\\oop\\dayfour\\jdemo\\text05.txt");
        // 判断文件是否存在
        if (file.exists())
        {
            file.delete();    // 存在则先删除
        }
        file.createNewFile();     // 再创建
        FileOutputStream fileOut=new FileOutputStream(file);
        OutputStreamWriter outputStreamWriter=new OutputStreamWriter(fileOut, "GBK");
        outputStreamWriter.write(str);
        outputStreamWriter.close();
        fileOut.close();
    }


    /**
     * 利用转换输入流将当前项目根目录下使用gbk编码的text05.txt文件的内容读取出来，并打印在控制台上。
     * @throws IOException
     */
    public void readStr() throws IOException {
        File file = new File("E:\\Ideawork\\lw_oop\\src\\com\\lw\\oop\\dayfour\\jdemo\\text05.txt");
        FileInputStream fileIn=new FileInputStream(file);
        InputStreamReader inputStreamReader=new InputStreamReader(fileIn,"GBK");
        int n;
        while ((n=inputStreamReader.read())!=-1){
            System.out.print((char)n);
        }
        inputStreamReader.close();
        fileIn.close();
    }

    /**
     * 从键盘录入一行字符串，利用字节打印流将该行字符串保存到当前项目根目录下的d.txt文件中。
     * @param string
     * @throws IOException
     */
    public void inputStr(String string) throws IOException {
        String url="E:\\Ideawork\\lw_oop\\src\\com\\lw\\oop\\dayfour\\jdemo\\d.txt";
        BufferedOutputStream buf=new BufferedOutputStream(new FileOutputStream(url));
        byte[] bytes=string.getBytes();
        buf.write(bytes);
        buf.close();
    }

    /**
     * 利用IO流的知识读取text.txt文件的内容反转后写入text1.txt文件中
     * @throws IOException
     */
    public void IoCopy() throws IOException {
        File file = new File("E:\\Ideawork\\lw_oop\\src\\com\\lw\\oop\\dayfour\\jdemo\\text.txt");
        FileReader fr=new FileReader(file);
        BufferedReader bufRea=new BufferedReader(fr);
        String[] strings = new String[100];
        String str=null;
        int i=0;
        while ((str = bufRea.readLine())!=null){
            strings[i]=str;
            i++;
        }
        File file1 = new File("E:\\Ideawork\\lw_oop\\src\\com\\lw\\oop\\dayfour\\jdemo\\text1.txt");
        // 判断文件是否存在
        if (file.exists())
        {
            file.delete();    // 存在则先删除
        }
        file.createNewFile();     // 再创建
        BufferedWriter br=new BufferedWriter(new FileWriter(file1));
        for (String s:strings) {
            if (s!=null){
                br.write(s);
            }
        }
        br.close();
        bufRea.close();
        fr.close();
    }


    /**
     * 创建文件data.txt
     * @throws IOException
     */
    public void creFile() throws IOException {
        File file = new File("E:\\Ideawork\\lw_oop\\src\\com\\lw\\oop\\dayfour\\jdemo\\data.txt");
        // 判断文件是否存在
        if (file.exists())
        {
            file.delete();    // 存在则先删除
        }
        file.createNewFile();     // 再创建
    }

}
