package com.lw.oop.dayfour.fdemo;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * @author liuwei
 * @ClassName CopyPictureTest.java
 * @description: 复制图片
 * @date 2023年06月18日
 * @version: 1.0
 */
public class CopyPictureTest {
    public static void main(String[] args) throws FileNotFoundException {
        //文件字节输入流
        FileInputStream fi=new FileInputStream("E:\\Ideawork\\lw_oop\\src\\com\\lw\\oop\\dayfour\\ademo\\file\\2023001.png");
        //文件字节输出流
        FileOutputStream fo=new FileOutputStream("E:\\Ideawork\\lw_oop\\src\\com\\lw\\oop\\dayfour\\ademo\\file\\2023copy.png");
        byte[] buf=new byte[1024];
        int count=0;
        try{
            while ((count = fi.read(buf))!=-1){
                fo.write(buf,0,count);
            }
        }catch (IOException e) {
            e.printStackTrace();
        }finally {
            try {
                fi.close();
                fo.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        System.out.println("复制完毕！");
    }
}
