package com.lw.oop.dayfour.hdemo;

import java.io.*;

/**
 * @author liuwei
 * @ClassName BuTest.java
 * @description: TODO
 * @date 2023年06月18日
 * @version: 1.0
 */
public class BuTest {
    public static void main(String[] args) throws IOException {
        String str="E:\\Ideawork\\lw_oop\\src\\com\\lw\\oop\\dayfour\\ademo\\file\\d.txt";
        File file=new File(str);
        FileInputStream in=new FileInputStream(file);
        BufferedInputStream bis=new BufferedInputStream(in);
        int len;
        byte[] bytes = new byte[128];
        while ((len=bis.read(bytes))!=-1){
            System.out.println(new String(bytes,0,len));
        }
        bis.close();
        in.close();
    }
}
