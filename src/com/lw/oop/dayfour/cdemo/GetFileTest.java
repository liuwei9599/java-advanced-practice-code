package com.lw.oop.dayfour.cdemo;

import java.io.File;

/**
 * @author liuwei
 * @ClassName GetFileTest.java
 * @description: 递归遍历所有文件
 * @date 2023年06月16日
 * @version: 1.0
 */
public class GetFileTest {
    static String s = " ";
    public static void getMyFiles2(File fileName) {
        File[] files = fileName.listFiles();
        if (files!=null){
            for (File te: files) {
                if (te.isFile()){
                    System.out.println(s+te.getName());
                }else if (te.isDirectory()){
                    System.out.println(te.getName());
                    getMyFiles2(te);
                }
            }
        }
        s+=s;
    }
    public static void main(String[] args) {
        String url = "E:\\Ideawork\\lw_oop\\src\\com\\lw\\oop\\dayfour";
        File dir = new File(url);
        getMyFiles2(dir);
    }
}
