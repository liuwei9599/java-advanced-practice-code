package com.lw.oop.dayfour.gdemo;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/**
 * @author liuwei
 * @ClassName SortTest.java
 * @description: TODO
 * @date 2023年06月18日
 * @version: 1.0
 */
public class SortTest {
    List<String> list1=new ArrayList<>();
    List<String> list2=new ArrayList<>();
    public void getMyFiles(File fileName) {
        File[] files = fileName.listFiles();
        if (files!=null){
            for (File te: files) {
                if (te.isFile()){
                    list2.add(te.getName());
                }else if (te.isDirectory()){
                    list1.add(te.getName());
                    getMyFiles(te);
                }
            }
        }
    }
    public static void main(String[] args) {
        SortTest sortTest = new SortTest();
        String s="E:\\Ideawork\\lw_oop\\src\\";
        File file=new File(s);
        sortTest.getMyFiles(file);
        Collections.sort(sortTest.list1);
        Collections.sort(sortTest.list2);
        System.out.println("文件夹：");
        Iterator it1=sortTest.list1.iterator();
        while (it1.hasNext()){
            System.out.println(it1.next());
        }
        System.out.println("文件：");
        for (int i =sortTest.list2.size()-1 ; i > 0; i--) {
            System.out.println(sortTest.list2.get(i));
        }
    }
}
