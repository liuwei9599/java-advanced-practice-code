package com.lw.oop.dayfour.edemo;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * @author liuwei
 * @ClassName OutputTest.java
 * @description: TODO
 * @date 2023年06月18日
 * @version: 1.0
 */
public class OutputTest {
    public static void main(String[] args) {
        //字符输出流
        String out2 = "E:\\Ideawork\\lw_oop\\src\\com\\lw\\oop\\dayfour\\ademo\\file\\HelloWorldTest.txt";
        FileWriter fw = null;
        try {
            File file = new File(out2);
            if (file.exists())
            {
                file.delete();    // 存在则先删除
            }
            file.createNewFile();     // 再创建
            fw = new FileWriter(file);
            String msg = "HelloJavaWorld你好世界";
            //写入字符串
            fw.write(msg);
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            try {
                fw.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
