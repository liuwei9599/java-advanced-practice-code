package com.lw.oop.dayfour.idemo;

import java.io.*;

/**
 * @author liuwei
 * @ClassName BufOutTest.java
 * @description: TODO
 * @date 2023年06月18日
 * @version: 1.0
 */
public class BufOutTest {
    public static void main(String[] args) throws IOException {
        String string="E:\\Ideawork\\lw_oop\\src\\com\\lw\\oop\\dayfour\\ademo\\file\\e.txt";
        FileOutputStream out=new FileOutputStream(string);
        BufferedOutputStream buf=new BufferedOutputStream(out);
        byte[] bytes="I love Java".getBytes();
        buf.write(bytes);
        buf.close();
        out.close();
    }
}
