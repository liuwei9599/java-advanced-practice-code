package com.lw.oop.dayfour.bdemo;

import java.io.File;
import java.io.IOException;

/**
 * @author liuwei
 * @ClassName HelloTest.java
 * @description: TODO
 * @date 2023年06月16日
 * @version: 1.0
 */
public class HelloTest {
    public static void main(String[] args) throws IOException {
        String url = "E:\\Ideawork\\lw_oop\\src\\com\\lw\\oop\\dayfour\\ademo\\file\\file1\\";
        File fl = new File(url, "HelloWorld.txt");
        if (fl.exists()){
            fl.delete();
        }
        fl.createNewFile();
        if (fl.isDirectory()){
            System.out.println(fl.getName()+"是目录");
        }else if(fl.isFile()){
            System.out.println(fl.getName()+"是文件");
        }
        String url1 = "E:\\Ideawork\\lw_oop\\src\\com\\lw\\oop\\dayfour\\ademo\\file\\";
        File file = new File(url1,"IOTest");
        if(file.exists()){
            file.delete();
        }
        file.mkdir();
        if (file.isDirectory()){
            System.out.println(file.getName()+"是目录");
        }else if(file.isFile()){
            System.out.println(file.getName()+"是文件");
        }

        if (fl.renameTo(new File(file,"HelloWorld.txt"))){
            System.out.println("移动文件成功！");
        }else {
            System.out.println("移动文件失败！");
        }
    }
}
