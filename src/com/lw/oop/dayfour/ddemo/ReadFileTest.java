package com.lw.oop.dayfour.ddemo;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 * @author liuwei
 * @ClassName ReadFileTest.java
 * @description: TODO
 * @date 2023年06月18日
 * @version: 1.0
 */
public class ReadFileTest {
    public static void main(String[] args) {
        String output = "E:\\Ideawork\\lw_oop\\src\\com\\lw\\oop\\dayfour\\ademo\\file\\test.txt";
        FileReader f = null;
        try {
            File file = new File(output);
            f = new FileReader(file);
            int len;
            char[] buf = new char[100];
            while ((len = f.read(buf)) != -1) {
                String s1 = new String(buf, 0, len);
                System.out.println(s1);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            try {
                f.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
