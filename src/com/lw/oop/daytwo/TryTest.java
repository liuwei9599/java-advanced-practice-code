package com.lw.oop.daytwo;

/**
 * @author liuwei
 * @ClassName TryTest.java
 * @description: TODO
 * @date 2023年06月14日
 * @version: 1.0
 */
public class TryTest {
    public static String test() {
        try {
            int num = 1 / 0;
            return "这是try";
        } catch (Exception e) {
            e.printStackTrace();
            return "这是catch";
        } finally {
            System.out.println("**finally**");
            return "这是finally";
        }
    }
    public static void main(String[] args) {
        /**
         * 测试try-catch-finally 和return的执行顺序
         */
        System.out.println(TryTest.test());
    }
}
