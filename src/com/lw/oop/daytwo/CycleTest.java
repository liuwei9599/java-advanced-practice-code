package com.lw.oop.daytwo;

import java.util.ArrayList;
import java.util.List;

/**
 * @author liuwei
 * @ClassName CycleTest.java
 * @description: TODO
 * @date 2023年06月13日
 * @version: 1.0
 */
public class CycleTest {
    public static void main(String[] args) {
        //for循环
        int n = 10;
        for (int i = 0; i < n; i++) {
            System.out.print(i + "\t");
        }
        System.out.println("\n***************************************");
        int i = 0;
        while (i < n) {
            System.out.print(i + "\t");
            i++;
        }
        System.out.println("\n***************************************");
        int j = 0;
        do {
            System.out.print(j + "\t");
            j++;
        } while (j < n);
        System.out.println("\n***************************************");
        List<String> list = new ArrayList<>();
        list.add("1");
        list.add("2");
        list.add("3");
        for (String s : list) {
            System.out.print(s + "\t");
        }
        System.out.println("\n***************************************");
        List<Integer> alist = new ArrayList<Integer>();
        alist.add(1);
        alist.add(2);
        alist.add(3);
        //forEach 循环
        alist.forEach(item -> {
            System.out.print("元素" + item);
        });
        System.out.println("\n***************************************");
        //forEach 循环
        alist.forEach(System.out::println);
    }

}
