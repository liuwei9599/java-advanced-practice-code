package com.lw.oop.daytwo;

/**
 * @author liuwei
 * @ClassName MultiplicationTables.java
 * @description: TODO
 * @date 2023年06月13日
 * @version: 1.0
 */
public class MultiplicationTables {
    public static void main(String[] args) {

        for (int i = 1; i < 10; i++) {
            for (int j = 1; j <=i; j++) {
                System.out.print(j+"*"+i+"="+(i * j)+"\t");
            }
            System.out.println();
        }
    }
}
