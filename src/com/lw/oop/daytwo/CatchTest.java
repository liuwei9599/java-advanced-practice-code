package com.lw.oop.daytwo;

import java.io.IOException;

/**
 * @author liuwei
 * @ClassName CatchTest.java
 * @description: TODO
 * @date 2023年06月14日
 * @version: 1.0
 */
public class CatchTest {

    public void aatest() throws Exception{

    }
    public static void bbtest() throws NullPointerException, IOException {
        String str = null;
        if (str == null) {
            throw new IOException("caverageo");
        }
        Object o = null;
        throw new NullPointerException("do not pass");
    }


    public static void main(String[] args) throws Exception {
        //CatchTest.bbtest();

        try {
            int x = 1 / 0;
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("异常");
        }
    }
}
