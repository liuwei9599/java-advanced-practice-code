package com.lw.oop.daythree.fdemo;

/**
 * @author liuwei
 * @ClassName ArrayTest.java
 * @description: TODO
 * @date 2023年06月15日
 * @version: 1.0
 */
public class ArrayTest {
    public static void main(String[] args) {
        //创建一维数组
        int a1[] = new int[5];
        int[] a = new int[5];

        int b1[] = new int[]{4, 7, 3, 9};
        int[] b2 = new int[]{4, 7, 3, 9};

        int c1[] = {1, 2, 3, 4, 5};
        int[] c2 = {1, 2, 3, 4, 5};
        //创建二维数组
        int[][] arr1 = new int[3][3];
        int arr2[][] = new int[3][3];

        int[][] arr3 = new int[][]{{1, 2, 3}, {4, 5,6}, {7, 8, 9}};
        int arr4[][]= new int[][]{{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};

        int[][] arr5 = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};
        int[][] arr6 = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};


        int[] num=new int[]{1, 2, 3, 4, 5};
        for (int n:num) {
            System.out.print(n+"\t");
        }
        System.out.println("\n******************");
        for (int i = 0; i < 5; i++) {
            System.out.print(num[i]+", ");
        }

    }
}
