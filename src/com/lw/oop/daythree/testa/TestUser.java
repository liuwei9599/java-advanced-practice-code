package com.lw.oop.daythree.testa;

import java.util.ArrayList;

/**
 * @author liuwei
 * @ClassName TestUser.java
 * @description: TODO
 * @date 2023年06月14日
 * @version: 1.0
 */
public class TestUser {
    public static void main(String[] args) throws Exception {
        Class userClass = Class.forName("com.lw.oop.daythree.testa.User");
        System.out.println("根据类名:  \t" + User.class);
        System.out.println("根据对象:  \t" + new User().getClass());
        System.out.println("根据全限定类名:\t" + Class.forName("com.lw.oop.daythree.testa.User"));
        System.out.println("获取全限定类名:\t" + userClass.getName());
        System.out.println("获取类名:\t" + userClass.getSimpleName());
        System.out.println("实例化:\t" + userClass.newInstance());



    }
    public static void fun(User< ? super ArrayList> temp, int x){    // 只能接收String或Object类型的泛型，String类的父类只有Object类
        System.out.print(temp + ", ") ;
    }
}
