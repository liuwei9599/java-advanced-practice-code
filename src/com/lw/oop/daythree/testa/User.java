package com.lw.oop.daythree.testa;

/**
 * @author liuwei
 * @ClassName User.java
 * @description: TODO
 * @date 2023年06月14日
 * @version: 1.0
 */
public class User<T> {


    public static <T> String m(T t1) {
        System.out.println("m方法运行的对象："+t1);
        return "";
    }
    private String name = "init";
    private int age;
    public User() {}
    public User(String name, int age) {
        super();
        this.name = name;
        this.age = age;
    }
    private String getName() {
        return name;
    }
    private void setName(String name) {
        this.name = name;
    }
    public int getAge() {
        return age;
    }
    public void setAge(int age) {
        this.age = age;
    }
    @Override
    public String toString() {
        return "User [name=" + name + ", age=" + age + "]";
    }
}
