package com.lw.oop.daythree.jdemo;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * @author liuwei
 * @ClassName MappTest.java
 * @description: TODO
 * @date 2023年06月15日
 * @version: 1.0
 */
public class MappTest {
    public static void main(String[] args) {
        Map<Integer, String> map = new HashMap<>(16);
        map.put(1, "张三丰");
        map.put(2, "周芷若");
        map.put(3, "汪峰");
        map.put(4, "灭绝师太");
        //遍历集合，并将序号与人名打印
        Set< Map.Entry<Integer, String>> set = map.entrySet();
        for (Map.Entry<Integer, String> entry: set) {
            System.out.println(entry.getKey() + "--------"+entry.getValue());
        }

        map.put(5, "郭靖");
        map.remove(1);
        map.replace(2, "周林");
        map.forEach((key , val) -> System.out.println("编号: " + key + "-----" + "姓名: " + val));
    }
}
