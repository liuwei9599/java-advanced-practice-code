package com.lw.oop.daythree.idemo;

import java.util.*;

/**
 * @author liuwei
 * @ClassName LinkTest.java
 * @description: TODO
 * @date 2023年06月15日
 * @version: 1.0
 */
public class LinkTest {
    public static void main(String[] args) {
        String[] strs = {"12345","67891","12347809933","98765432102","67891","12347809933"};
        List<String> list = new LinkedList<>();
        Set<String> set = new HashSet<>();
        for (int i = 0; i <strs.length ; i++) {
            list.add(strs[i]);
        }
        for (String str: list) {
            System.out.print(str + ", ");
        }
        System.out.println("\n**********************");
        set.addAll(list);
        Iterator<String> iterator = set.iterator();
        while (iterator.hasNext()) {
            System.out.print(iterator.next() + ", ");
        }

    }
}
