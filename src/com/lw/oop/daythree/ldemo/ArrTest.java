package com.lw.oop.daythree.ldemo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/**
 * @author liuwei
 * @ClassName ArrTest.java
 * @description: TODO
 * @date 2023年06月15日
 * @version: 1.0
 */
public class ArrTest {
    public static void main(String[] args) {
        List<String> list = new ArrayList<String>();
        list.add("ital");
        list.add("java");
        list.add("c#");
        list.add("f#");
        list.add("stop");
        System.out.println("list的元素个数："+list.size());
        //排序
        Collections.sort(list);
        System.out.println("第二个元素是："+ list.get(1));
        System.out.println("第三个元素是："+list.get(2));
        list.remove(3);
        System.out.println("list:"+list);
        list.set(2, "TTTTT");
        Iterator<String> iterator = list.iterator();
        while (iterator.hasNext()) {
            System.out.print(iterator.next()+"\t");
        }
    }
}
