package com.lw.oop.daythree.bdemo;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Scanner;

/**
 * @author liuwei
 * @ClassName HashTest.java
 * @description: TODO
 * @date 2023年06月14日
 * @version: 1.0
 */
public class HashTest {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        HashMap<String, Integer> h = new HashMap<>();
        System.out.println("请输入学生个数：");
        Integer num = sc.nextInt();
        for (int i = 1; i <= num; i++) {
            System.out.println("请输入" + i + "个学生的姓名：");
            String name = sc.next();
            System.out.println("请输入" + name + "学生的成绩：");
            Integer c = sc.nextInt();
            h.put(name, c);
        }
        System.out.println("请输入要查询的学生姓名：");
        String name = sc.next();
        System.out.println(name + "的成绩是:" + h.get(name));
        System.out.println("所有的学生的成绩是:");
        h.forEach((n, c) -> {
            System.out.println(n + "--------" + c);
        });
        /**
         * foreach输出
         */
        // 打印键集合
        for (String key : h.keySet()) {
            System.out.println("姓名："+key+"\t");
        }
        // 打印值集合
        for (Integer value : h.values()) {
            System.out.println("成绩："+value+"\t");
        }

        /**
         * HashMap集合的遍历，使用增强for循环
         */
        System.out.println("Map集合的遍历，使用增强for循环");
        for (HashMap.Entry<String, Integer> entry: h.entrySet()) {
            System.out.println(entry.getKey() + "--------"+entry.getValue());
        }

        /**
         * Hashmap集合遍历，迭代器
         */
        System.out.println("Map集合的遍历，迭代器");
        Iterator iter = h.entrySet().iterator();
        while (iter.hasNext()) {
            System.out.println(iter.next());
        }
    }
}







