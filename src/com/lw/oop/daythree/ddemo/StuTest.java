package com.lw.oop.daythree.ddemo;

import java.util.*;

/**
 * @author liuwei
 * @ClassName StuTest.java
 * @description: TODO
 * @date 2023年06月15日
 * @version: 1.0
 */
public class StuTest {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        List<Student> stu1 = new ArrayList<>();
        stu1.add(new Student("刘备", "男", 23));
        stu1.add(new Student("张飞", "男", 33));
        stu1.add(new Student("关羽", "男", 30));
        List<Student> stu2 = new ArrayList<>();
        stu2.add(new Student("张无忌", "男", 22));
        stu2.add(new Student("张三丰", "男", 27));
        stu2.add(new Student("杨逍", "男", 27));
        List<Student> stu3 = new ArrayList<>();
        stu3.add(new Student("孙悟空", "男", 18));
        stu3.add(new Student("猪八戒", "男", 15));
        stu3.add(new Student("沙和尚", "男", 20));

        Map<String, List< Student>> stuMap = new HashMap<>();
        stuMap.put("三年级一班", stu1);
        stuMap.put("三年级二班", stu2);
        stuMap.put("三年级三班", stu3);
        System.out.println("请输入班级名称 ：");
        String className = sc.next();
        if (stuMap.containsKey(className)){
            System.out.println(className+"的学生列表：");
            List< Student> li = stuMap.get(className);
            Iterator<Student> it = li.iterator();
            while (it.hasNext()) {
                Student p = it.next();
                System.out.println(p.toString());
            }
        }else {
            System.out.println("没有找到这个班级！！！");
        }








    }
}
