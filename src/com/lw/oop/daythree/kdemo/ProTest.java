package com.lw.oop.daythree.kdemo;

import java.util.HashMap;
import java.util.Map;

/**
 * @author liuwei
 * @ClassName ProTest.java
 * @description: TODO
 * @date 2023年06月15日
 * @version: 1.0
 */
public class ProTest {
    public static void main(String[] args) {
        String[] a = new String[]{"黑龙江省", "浙江省", "江西省", "广东省", "福建省"};
        String[] b = new String[]{"哈尔滨", "杭州", "南昌", "广州", "福州"};
        Map<String, String> map = new HashMap<>();
        for (int i = 0; i < a.length; i++) {
            if (a[i] != null) {
                map.put(a[i], b[i]);
            }
        }
        System.out.println(map);

    }
}
