package com.lw.oop.daythree.gdemo;

import java.util.*;

/**
 * @author liuwei
 * @ClassName CollectionTest.java
 * @description: Arraylist 工具类方法使用
 * @date 2023年06月15日
 * @version: 1.0
 */
public class CollectionTest {
    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        for (int i = 2021; i <= 2025; i++) {
            list.add("admin"+i);
        }
        //遍历 list
        Iterator it = list.iterator();
        while (it.hasNext()) {
            String t = it.next().toString();
            System.out.print(t+"\t");
        }
        System.out.println("\n*********************************");

        //向列表尾部添加元素
        list.add("孙悟空001");
        //在列表的指定位置添加元素
        list.add(0, "沙悟净");
        //将指定collection中的所有元素都插入到列表中
        list.addAll(Arrays.asList("nullable", "java", "lambda"));
        //将指定collection中的所有元素都插入到列表中的指定位置
        list.addAll(0, Arrays.asList("Hello"));
        //如果列表中包含指定元素 ，则返回true
        if(list.contains("Hello")){
            System.out.println("列表中包含了"+"Hello");
        }

        //如果列表中包含指定collection的所有元素 ，则返回true
        System.out.println(list.containsAll(Arrays.asList("nullable", "java", "lambda")));
        //比较指定对象与列表是否相等   --false
        System.out.println(list.equals(Arrays.asList("nullable", "java", "lambda")));
        //返回列表中指定位置的元素
        System.out.println(list.get(1));
        //返回列表的哈希码值
        System.out.println(list.hashCode());
        //返回列表中首次出现指定元素的索引， 如果不存在，则返回-1
        System.out.println(list.indexOf("viewport"));
        //返回列表最后一次出现指定元素的索引， 如果不存在，则返回-1
        System.out.println(list.lastIndexOf("viewport"));
        //判断集合是否为空
        System.out.println(list.isEmpty());
        //返回正确顺序在列表的元素上的迭代器对象
        Iterator i = list.iterator();
        while (i.hasNext()) {
            System.out.print(i.next() + "\t");
        }
        System.out.println("\n***************************");
        //返回列表中元素的列表迭代器
        ListIterator sListIterator = list.listIterator();
        while (sListIterator.hasNext()) {
            System.out.print(sListIterator.next() + "\t");
        }
        System.out.println("\n******************************");
        //返回列表中元素的列表迭代器，从列表指定位置开始
        ListIterator sListIterator2 = list.listIterator(1);
        while (sListIterator2.hasNext()) {
            System.out.print(sListIterator2.next() + "\t");
        }
        //用指定元素替换列表中指定位置的元素
        list.set(1, "null");
        //返回列表中的元素个数
        System.out.println(list.size());
        //返回列表中指定的forIndex和toIndex之间的部分视图
        System.out.println(list.subList(0, 3));
        //返回以正确顺序包含列表中的所有元素的数组
        String[] stringArray = list.toArray(new String[list.size()]);
        System.out.println(stringArray);
        //移除列表指定位置的元素
        list.remove(2);
        //移除列表中出现的首个元素
        list.remove("nullable");
        list.forEach(System.out::println);
        //从列表中移除指定collection中的所有元素
        list.removeAll(Arrays.asList("nullable", "java", "lambda"));
        //仅保留指定collection中包含的元素
        list.retainAll(Arrays.asList("java", "Hello", "lambda"));

        //从列表中移除所有元素
        list.clear();
        //forEach遍历
        list.forEach(System.out::println);
    }
}
