package com.lw.oop.daythree.gdemo;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 * @author liuwei
 * @ClassName HashSetTest.java
 * @description: TODO
 * @date 2023年06月15日
 * @version: 1.0
 */
public class HashSetTest {
    public static void main(String[] args) {
        Set<String> set = new HashSet<>();
        for (int i = 0; i < 5; i++) {
            set.add("admin" + i);
        }
        set.add("admin1");
        set.add(null);

        System.out.println(set);
        //HashSet的长度
        System.out.println(set.size());
        //如果此集合不包含任何元素，则返回true。
        System.out.println(set.isEmpty());
        // 如果此集合包含所有指定的元素，则返回true。
        System.out.println(set.contains("admin"));
        //移除一个元素
        System.out.println(set.remove("admin"));
        //从此集合中移除所有元素，此调用返回值，该集合将为空
        set.clear();

        //遍历set集合
        Iterator it = set.iterator();
        while (it.hasNext()) {
            System.out.println(it.next());
        }


    }
}
