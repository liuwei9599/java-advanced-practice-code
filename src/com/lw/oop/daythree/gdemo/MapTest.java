package com.lw.oop.daythree.gdemo;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 * @author liuwei
 * @ClassName MapTest.java
 * @description: TODO
 * @date 2023年06月15日
 * @version: 1.0
 */
public class MapTest {
    public static void main(String[] args) {
        Map<Integer, String> map = new HashMap<>(10);
        map.put(0, "I like things");
        map.put(1, "clean");
        map.put(2, "java");
        map.put(3, "poor");
        map.put(4, "finishing");
        map.put(5, "clean");
        System.out.println(map);
        map.forEach((k, v) -> System.out.println(k+"="+v));
        //获取集合中元素的个数
        System.out.println(map.size());
        //判断集合元素是否为0
        System.out.println(map.isEmpty());
        // 判断 map集合是否包含指定Key
        System.out.println(map.containsKey(5));
        // 判断 map集合是否包含指定Value
        System.out.println(map.containsValue("finishing"));
        //获取集合中所以的Key，返回 一个包含所有 Key的Set集合
        System.out.println(map.keySet());
        // 获取集合中所以的Value，返回 一个包含所有 Value的Collection集合
        System.out.println(map.values());
        //修改键值对<key, oldValue>的value为newValue
        map.replace(1, "clean", "Html");
        //遍历map
        Iterator< Map.Entry<Integer, String>> iter = map.entrySet().iterator();
        while (iter.hasNext()) {
            Map.Entry<Integer, String> entry = iter.next();
            System.out.println(entry.getKey() + "--------"+entry.getValue());
        }
        //将map集合转换成set集合
        Set<Integer> keys = map.keySet();
        System.out.println(keys);
        // 删除元素
        map.remove(3);
        //清空 map集合
        map.clear();
    }

}
