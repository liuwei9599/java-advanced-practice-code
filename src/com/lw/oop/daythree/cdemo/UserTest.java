package com.lw.oop.daythree.cdemo;

import java.util.*;

/**
 * @author liuwei
 * @ClassName UserTest.java
 * @description: TODO
 * @date 2023年06月14日
 * @version: 1.0
 */
public class UserTest {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        List<String> user1 = new ArrayList<>();
        List<String> user2 = new ArrayList<>();
        List<String> user3 = new ArrayList<>();
        user1.add("1student001");
        user1.add("1student002");
        user1.add("1student003");

        user2.add("2student001");
        user2.add("2student002");
        user2.add("2student003");

        user3.add("3student001");
        user3.add("3student002");
        user3.add("3student003");

        Map<String, List<String>> map = new HashMap<>();
        map.put("三年级一班", user1);
        map.put("三年级二班", user2);
        map.put("三年级三班", user3);
        System.out.println("请输入班级:");
        String className = scanner.next();

        if (map.containsKey(className)) {
            System.out.println(className + "的学生列表：");
            List<String> list = map.get(className);
            list.forEach(System.out::println);
        } else {
            System.out.println("输入错误");
        }

        System.out.println("**************************************");
        System.out.println(className + "的学生列表：");

        List<String> list1 = map.getOrDefault(className , new ArrayList<>());

        Iterator it1 = list1.iterator();
        while (it1.hasNext()) {
            System.out.println(it1.next());
        }
//        if (className.equals("三年级一班")) {
//            System.out.println(className+"的学生列表：");
//            List<String> list1 = map.get(className);
//            Iterator it1 = list1.iterator();
//            while (it1.hasNext()) {
//                System.out.println(it1.next());
//            }
//        } else if (className.equals("三年级二班")) {
//            System.out.println(className+"的学生列表：");
//            List<String> list2 = map.get(className);
//            Iterator it2 = list2.iterator();
//            while (it2.hasNext()) {
//                System.out.println(it2.next());
//            }
//        } else if (className.equals("三年级三班")){
//            System.out.println(className+"的学生列表：");
//            List<String> list3 = map.get(className);
//            Iterator it3 = list3.iterator();
//            while (it3.hasNext()) {
//                System.out.println(it3.next());
//            }
//        }


    }
}
