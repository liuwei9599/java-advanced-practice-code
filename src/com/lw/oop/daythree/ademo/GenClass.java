package com.lw.oop.daythree.ademo;

/**
 * @author liuwei
 * @ClassName GenClass.java
 * @description: TODO
 * @date 2023年06月14日
 * @version: 1.0
 */
public class GenClass<T, V> {
    T t;
    V v;

    public static <T, V> Boolean comp(T t,V v){
        return t.equals(v);
    }
    public T getT() {
        return t;
    }

    public void setT(T t) {
        this.t = t;
    }

    public V getV() {
        return v;
    }

    public void setV(V v) {
        this.v = v;
    }
}
