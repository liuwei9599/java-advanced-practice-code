package com.lw.oop.daythree.ademo;

/**
 * @author liuwei
 * @ClassName Status.java
 * @description: TODO
 * @date 2023年06月14日
 * @version: 1.0
 */
public enum  Status {
    /**
     * 已删除状态
     */
    HAVE_DELETE_STATUS(0, "已删除"),
    /**
     * 不是删除状态
     */
    NOT_DELETE_STATUS(1, "未删除");
    private Integer status;
    private String desc;
    Status(Integer status, String desc) {
        this.status = status;
        this.desc = desc;
    }
    public Integer getStatus() {
        return status;
    }
    public void setStatus(Integer status) {
        this.status = status;
    }
    public String getDesc() {
        return desc;
    }
    public void setDesc(String desc) {
        this.desc = desc;
    }
    public static void main(String[] args) {
        Integer num=0;
        if (Status.HAVE_DELETE_STATUS.getStatus().equals(num)){
            String str = Status.HAVE_DELETE_STATUS.getDesc();
            System.out.println(str);
        }
    }
}
