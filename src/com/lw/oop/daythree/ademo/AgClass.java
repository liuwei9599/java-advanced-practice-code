package com.lw.oop.daythree.ademo;

/**
 * @author liuwei
 * @ClassName AgClass.java
 * @description: TODO
 * @date 2023年06月14日
 * @version: 1.0
 */
public class AgClass {
    public static <T> T comp(User<? extends Number> t) {
        return (T) t.getT();
    }

    public <T> Integer comp2(User<? super Number> t) {
        return 1;
    }

    public static <T, V> String comp2(GenClass<? super String, V> item, V v) {
        return (String) item.getT() + v;
    }

    public static void main(String[] args) {
        GenClass<String, Integer> genClass = new GenClass<>();
        genClass.setT("Strings");
        genClass.setV(1);
        System.out.println(AgClass.comp2(genClass, 1));

        User<Integer> user = new User<>();
        user.setT(100);
        Integer comp = AgClass.comp(user);
        System.out.println(comp);
    }

}
