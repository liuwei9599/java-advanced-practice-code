package com.lw.oop.daythree.ademo;

/**
 * @author liuwei
 * @ClassName User.java
 * @description: TODO
 * @date 2023年06月14日
 * @version: 1.0
 */
public class User<T> {
    T t;

    public T getT() {
        return t;
    }

    public void setT(T t) {
        this.t = t;
    }
}
