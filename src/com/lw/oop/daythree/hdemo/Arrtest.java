package com.lw.oop.daythree.hdemo;

import java.util.ArrayList;
import java.util.List;

/**
 * @author liuwei
 * @ClassName Arrtest.java
 * @description: TODO
 * @date 2023年06月15日
 * @version: 1.0
 */
public class Arrtest {
    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>();
        int[] a = new int[10];
        int max = 100;
        int min=1;
        for (int i = 0; i <a.length ; i++) {
            a[i]=min + (int) (Math.random() * (max - min));
        }
        System.out.println("********************************");
        for (int n: a) {
            System.out.print(n+", ");
        }
        for (int i = 0; i <a.length ; i++) {
            if (a[i]>=10){
                list.add(a[i]);
            }
        }
        System.out.println("\n**********************************");
        for (int n: list) {
            System.out.print(n+", ");
        }
    }
}
