package com.lw.oop.dayone.odemo;

/**
 * @author liuwei
 * @ClassName Vehicle.java
 * @description: TODO
 * @date 2023年06月13日
 * @version: 1.0
 */
public abstract class Vehicle {

    /**
     * 轮子
     * @return {@link String}
     */
    public abstract String noOfWheels();
}
