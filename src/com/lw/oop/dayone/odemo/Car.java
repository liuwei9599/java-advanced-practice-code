package com.lw.oop.dayone.odemo;

/**
 * @author liuwei
 * @ClassName Car.java
 * @description: TODO
 * @date 2023年06月13日
 * @version: 1.0
 */
public class Car extends Vehicle{
    @Override
    public String noOfWheels() {
        String res = "四轮车";
        return res;
    }
}
