package com.lw.oop.dayone.odemo;

/**
 * @author liuwei
 * @ClassName VeTest.java
 * @description: TODO
 * @date 2023年06月13日
 * @version: 1.0
 */
public class VeTest {

    public static void main(String[] args) {
        Vehicle v1 = new Motorbike();
        Vehicle v2 = new Car();
        System.out.println(v1.noOfWheels());
        System.out.println(v2.noOfWheels());
    }

}
