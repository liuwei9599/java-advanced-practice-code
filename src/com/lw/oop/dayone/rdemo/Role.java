package com.lw.oop.dayone.rdemo;

/**
 * @author liuwei
 * @ClassName Role.java
 * @description: TODO
 * @date 2023年06月13日
 * @version: 1.0
 */
public abstract class Role {
    private String name;
    private Integer age;
    private String sex;
    private String address;
    private String tel;

    public Role() {
    }

    public Role(String name, Integer age, String sex) {
        this.name = name;
        this.age = age;
        this.sex = sex;
    }

    public Role(String name, Integer age, String sex, String address, String tel) {
        this.name = name;
        this.age = age;
        this.sex = sex;
        this.address = address;
        this.tel = tel;
    }
    public abstract void play();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }
}
