package com.lw.oop.dayone.rdemo;

/**
 * @author liuwei
 * @ClassName RoTest.java
 * @description: TODO
 * @date 2023年06月13日
 * @version: 1.0
 */
public class RoTest {
    public static void main(String[] args) {
        Employee b = new Employee( "xaioxiao", 19, "男",5000.0);
        b.play();
        b.sing();
        Manager m = new Manager( "nice", 18, "女", 2000.0);
        m.play();
        m.sing();
    }
}
