package com.lw.oop.dayone.rdemo;

/**
 * @author liuwei
 * @ClassName Manager.java
 * @description: TODO
 * @date 2023年06月13日
 * @version: 1.0
 */
public class Manager extends Employee{
    private final String vehicle= "朗驰";

    public Manager( String name, Integer age, String sex, Double salary) {
        super(name, age, sex, salary);
    }

    public Manager( String name, Integer age, String sex, String address, String tel, Double salary) {
        super( name, age, sex, address, tel, salary);
    }

    @Override
    public void play() {
        System.out.println("公司："+vehicle);
        super.play();
    }
}
