package com.lw.oop.dayone.rdemo;

/**
 * 员工
 *
 * @author liuwei
 * @ClassName Employee.java
 * @description: TODO
 * @date 2023年06月13日
 * @version: 1.0
 */
public class Employee extends Role{
    /**
     * 员工编号
     */
    private static String eid="20230001";
    /**
     * 工资
     */
    private Double salary;

    /**
     * 员工
     *
     * @param name   名字
     * @param age    年龄
     * @param sex    性别
     * @param salary 工资
     */
    public Employee(String name, Integer age, String sex, Double salary) {
        super(name, age, sex);
        this.salary = salary;
    }

    /**
     * 员工
     *
     * @param name    名字
     * @param age     年龄
     * @param sex     性别
     * @param address 地址
     * @param tel     电话
     * @param salary  工资
     */
    public Employee(String name, Integer age, String sex, String address, String tel, Double salary) {
        super(name, age, sex, address, tel);
        this.salary = salary;
    }

    /**
     * 玩
     */
    @Override
    public void play() {
        System.out.println("职工编号ID:"+eid+"\t员工姓名:"+getName()+ "\t员工地址:"+getAddress()+
                "\t员工年龄:"+getAge()+"\t员工性别:"+ getSex()+"\t员工工资:"+salary);
    }

    /**
     * 唱歌
     */
    public final void sing(){
        System.out.println("爱好唱歌");
    }

    /**
     * 得到工资
     *
     * @return {@link Double}
     */
    public Double getSalary() {
        return salary;
    }

    /**
     * 设定薪资
     *
     * @param salary 工资
     */
    public void setSalary(Double salary) {
        this.salary = salary;
    }
}
