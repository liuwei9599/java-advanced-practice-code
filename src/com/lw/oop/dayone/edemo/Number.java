package com.lw.oop.dayone.edemo;

/**
 * @author liuwei
 * @ClassName Number.java
 * @description: TODO
 * @date 2023年06月13日
 * @version: 1.0
 */
public class Number {
    private Integer n1;
    private Integer n2;

    public Number() {

    }

    public Number(Integer n1, Integer n2) {
        this.n1 = n1;
        this.n2 = n2;
    }

    public Integer add(){
        Integer n = n1 + n2;
        return n;
    }
    public Integer sub() {
        Integer n = n1 - n2;
        return n;
    }
    public Integer mul() {
        Integer n = n1 * n2;
        return n;
    }
    public Integer div() {
        Integer n = n1 / n2;
        return n;
    }




}
