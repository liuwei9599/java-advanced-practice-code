package com.lw.oop.dayone.edemo;

/**
 * @author liuwei
 * @ClassName NumberTest.java
 * @description: TODO
 * @date 2023年06月13日
 * @version: 1.0
 */
public class NumberTest {
    public static void main(String[] args) {
        Integer n1 = 99;
        Integer n2 = 3;
        Number number = new Number(n1, n2);
        System.out.println(n1 + "+" + n2 + "=" + number.add());
        System.out.println(n1 + "-" + n2 + "=" + number.sub());
        System.out.println(n1 + "*" + n2 + "=" + number.mul());
        System.out.println(n1 + "/" + n2 + "=" + number.div());
    }
}
