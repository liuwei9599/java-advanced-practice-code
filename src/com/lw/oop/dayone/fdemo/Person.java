package com.lw.oop.dayone.fdemo;

/**
 * @author liuwei
 * @ClassName Person.java
 * @description: TODO
 * @date 2023年06月13日
 * @version: 1.0
 */
public class Person {
    private String name;
    private Integer age;

    public Person() {}
    public Person(String name, Integer age) {
        this.name = name;
        this.age = age;
    }
    public void disPlay(){
        System.out.println("姓名："+ name + "\t年龄："+age);
    }
}
