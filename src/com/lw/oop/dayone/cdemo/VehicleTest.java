package com.lw.oop.dayone.cdemo;

import java.util.Scanner;

/**
 * @author liuwei
 * @ClassName VehicleTest.java
 * @description: TODO
 * @date 2023年06月13日
 * @version: 1.0
 */
public class VehicleTest {
    public static void main(String[] ar) {
        Scanner sc = new Scanner(System.in);
        Vehicle vehicle = new Vehicle();
        System.out.print("请输入 速度：");
        double speed = sc.nextDouble();
        System.out.print("请输入行驶时间：");
        int runTime = sc.nextInt();
        vehicle.setRunningTime(runTime);
        vehicle.setSpeed(speed);
        System.out.println("当前速度为： "+ vehicle.getSpeed());
        System.out.println(vehicle.getRunningTime()+"小时行驶了"+vehicle.move());
        vehicle.speedUp();
        System.out.println("加速后的速度为： "+vehicle.getSpeed());
        vehicle.speedDown();
        System.out.println("减速后的速度为： "+vehicle.getSpeed());
    }
}
