package com.lw.oop.dayone.cdemo;

/**
 * @author liuwei
 * @ClassName Vehicle.java
 * @description: TODO
 * @date 2023年06月13日
 * @version: 1.0
 */
public class Vehicle {
    /**
     * 速度
     */
    private double speed;
    /**
     * 体积
     */
    private Integer size;

    /**
     * 行驶时间
     */
    private Integer runningTime;

    /**
     * 距离
     */
    private double distance;


    /**
     * 行驶
     */
    public double move() {
        distance= distance + speed*runningTime;
        return distance;
    }

    /**
     * 加速
     */
    public void speedUp(){
        speed = speed + 5;
    }

    /**
     * 减速
     */
    public void speedDown() {
        speed = speed -5;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(Integer distance) {
        this.distance = distance;
    }

    public Integer getRunningTime() {
        return runningTime;
    }

    public void setRunningTime(Integer runningTime) {
        this.runningTime = runningTime;
    }
    public double getSpeed() {
        return speed;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }
}
