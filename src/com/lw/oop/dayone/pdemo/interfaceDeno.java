package com.lw.oop.dayone.pdemo;

/**
 * @author liuwei
 * @ClassName interfaceDeno.java
 * @description: TODO
 * @date 2023年06月13日
 * @version: 1.0
 */
public class interfaceDeno {
    public static void main(String[] args) {
        Vehicle b = new Bike();
        Vehicle m = new Bus();
        m.start();
        b.start();
        b.stop();
        m.stop();

    }
}
