package com.lw.oop.dayone.pdemo;

/**
 * @author liuwei
 * @ClassName Bus.java
 * @description: TODO
 * @date 2023年06月13日
 * @version: 1.0
 */
public class Bus implements Vehicle{
    @Override
    public void start() {
        System.out.println("公交车正在行驶！");
    }

    @Override
    public void stop() {
        System.out.println("公交车停止！");
    }
}
