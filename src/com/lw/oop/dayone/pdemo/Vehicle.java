package com.lw.oop.dayone.pdemo;

/**
 * 车辆
 *
 * @author liuwei
 * @ClassName Vehicle.java
 * @description: TODO
 * @date 2023年06月13日
 * @version: 1.0
 */
public interface Vehicle {
    /**
     * 开始
     */
    void start();

    /**
     * 停止
     */
    void stop();
}
