package com.lw.oop.dayone.pdemo;

/**
 * @author liuwei
 * @ClassName Bike.java
 * @description: TODO
 * @date 2023年06月13日
 * @version: 1.0
 */
public class Bike implements Vehicle{
    @Override
    public void start() {
        System.out.println("自行车正在行驶！");
    }

    @Override
    public void stop() {
        System.out.println("自行车停止！");
    }
}
