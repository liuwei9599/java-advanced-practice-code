package com.lw.oop.dayone.hdemo;

/**
 * @author liuwei
 * @ClassName WuMingFen.java
 * @description: TODO
 * @date 2023年06月13日
 * @version: 1.0
 */
public class WuMingFen {
    private String theMa;
    private Integer quantity;
    private Boolean likeDoup;

    public WuMingFen() {}

    public WuMingFen(String theMa, Integer quantity) {
        this.theMa = theMa;
        this.quantity = quantity;
    }

    public WuMingFen(String theMa, Integer quantity, Boolean likeDoup) {
        this.theMa = theMa;
        this.quantity = quantity;
        this.likeDoup = likeDoup;
    }

    public void check(){
        System.out.println("面名："+ theMa);
        System.out.println("份数："+quantity);
        if (likeDoup!=null){
            if(likeDoup.equals(true)){
                System.out.println("带汤");
            }else {
                System.out.println("不带汤");
            }
        }else {
            System.out.println("不带汤");
        }

    }

    public String getTheMa() {
        return theMa;
    }

    public void setTheMa(String theMa) {
        this.theMa = theMa;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Boolean getLikeDoup() {
        return likeDoup;
    }

    public void setLikeDoup(Boolean likeDoup) {
        this.likeDoup = likeDoup;
    }
}
