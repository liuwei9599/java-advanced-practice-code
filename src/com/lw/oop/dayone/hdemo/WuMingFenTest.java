package com.lw.oop.dayone.hdemo;

/**
 * @author liuwei
 * @ClassName WuMingFenTest.java
 * @description: TODO
 * @date 2023年06月13日
 * @version: 1.0
 */
public class WuMingFenTest {
    public static void main(String[] args) {
        WuMingFen f1 = new WuMingFen("牛肉面", 2,true);
        f1.check();
        System.out.println("*******************************");
        WuMingFen f2 = new WuMingFen("牛肉面", 3);
        f2.check();
        System.out.println("********************************");
        WuMingFen f3 = new WuMingFen();
        f3.setTheMa("酸辣面");
        f3.setQuantity(2);
        f3.setLikeDoup(true);
        f3.check();
    }
}
