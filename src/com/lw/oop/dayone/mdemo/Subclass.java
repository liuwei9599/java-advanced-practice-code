package com.lw.oop.dayone.mdemo;

/**
 * @author liuwei
 * @ClassName Subclass.java
 * @description: TODO
 * @date 2023年06月13日
 * @version: 1.0
 */
public class Subclass extends ParentClass {
    public Subclass() {
        super(12, "enaocxiao");
    }
    public Subclass(Integer id, String name){
        super(id, name);

    }
    @Override
    public void disPlay() {
        super.disPlay();
        System.out.println("id："+id + ","+"姓名："+name);
        System.out.println("id："+super.getId() + ","+"姓名："+super.getName());
    }

}
