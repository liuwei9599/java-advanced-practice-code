package com.lw.oop.dayone.mdemo;

/**
 * @author liuwei
 * @ClassName ParentClass.java
 * @description: TODO
 * @date 2023年06月13日
 * @version: 1.0
 */
public class ParentClass {
    Integer id;
    String name;

    public ParentClass() {
    }
    public ParentClass(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void disPlay() {
        System.out.println("我是父类！");
        System.out.println("id："+this.id + ","+"姓名："+this.name);
    }
}
