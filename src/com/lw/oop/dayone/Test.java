package com.lw.oop.dayone;

/**
 * 测试
 *
 * @author lw
 * @ClassName Test.java
 * @Description
 * @createTime 2023年06月12日 18:44:00
 * @date 2023/06/12
 */

public class Test {

    /**
     * 名字
     */
    private String name;

    /**
     * t测试
     * 成员方法
     */
    public void tTest() {
        System.out.println("这是测试方法！");
    }

    /**
     * 主要
     *
     * @param
     */
    public static void main(String[] args) {
        Test t = new Test();
        try{

        }catch (Exception e){
            System.out.println("出现了异常！");
        }

        t.tTest();
    }
}
