package com.lw.oop.dayone.jdemo;

/**
 * @author liuwei
 * @ClassName AdditionTest.java
 * @description: TODO
 * @date 2023年06月13日
 * @version: 1.0
 */
public class AdditionTest {
    public static void main(String[] args) {
        Addition addition = new Addition();
        System.out.println(addition.add(10, 20));
        System.out.println(addition.add(100L, 200L));
        System.out.println(addition.add(10.01f, 20.01f));
        System.out.println(addition.add(10.11, 20.11));
        System.out.println(addition.add("hello", "world"));
    }
}
