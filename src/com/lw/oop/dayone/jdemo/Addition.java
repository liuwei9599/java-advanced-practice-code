package com.lw.oop.dayone.jdemo;

/**
 * @author liuwei
 * @ClassName Addition.java
 * @description: TODO
 * @date 2023年06月13日
 * @version: 1.0
 */
public class Addition {
    public Integer add(Integer a, Integer b){
        Integer sum= a+b;
        return sum;
    }
    public Long add(Long a, Long b) {
        Long sum = a+b;
        return sum;
    }
    public Float add(Float a, Float b) {
        Float sum = a+b;
        return sum;
    }
    public Double add(Double a, Double b) {
        Double sum = a+b;
        return sum;
    }
    public String add(String a, String b) {
        String sum = a+b;
        return sum;
    }

}
