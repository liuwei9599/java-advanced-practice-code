package com.lw.oop.dayone.gdemo;

/**
 * @author liuwei
 * @ClassName GsTest.java
 * @description: TODO
 * @date 2023年06月13日
 * @version: 1.0
 */
public class GsTest {
    public static void main(String[] args) {
        Student s = new Student();
        s.setId(2023001);
        s.setName("小小");
        s.setAge(20);
        s.setSex("男");
        s.setCla("一班");
        System.out.println(s.toString());
    }

}
