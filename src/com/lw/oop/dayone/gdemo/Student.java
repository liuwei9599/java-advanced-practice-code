package com.lw.oop.dayone.gdemo;

/**
 * @author liuwei
 * @ClassName Student.java
 * @description: TODO
 * @date 2023年06月13日
 * @version: 1.0
 */
public class Student {
    private Integer id;
    private String name;
    private Integer age;
    private String sex;
    private String cla;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getCla() {
        return cla;
    }

    public void setCla(String cla) {
        this.cla = cla;
    }

    @Override
    public String toString() {
        return "学生信息" +
                "\nid=" + id +
                "\nname=" + name +
                "\nage=" + age +
                "\nsex=" + sex +
                "\ncla=" + cla;
    }
}
