package com.lw.oop.dayone.ndemo;

/**
 * @author liuwei
 * @ClassName Square.java
 * @description: TODO
 * @date 2023年06月13日
 * @version: 1.0
 */
public class Square extends Graphics {
    Integer a,b;

    public Square(String name,Integer a, Integer b) {
        super(name);
        this.a = a;
        this.b = b;
    }

    @Override
    public void draw() {

        if (a.equals(b)){
            System.out.println("画一个正方形");
        }else {
            System.out.println("画一个长方形");
        }
    }
}
