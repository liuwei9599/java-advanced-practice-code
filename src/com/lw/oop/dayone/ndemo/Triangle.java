package com.lw.oop.dayone.ndemo;

/**
 * @author liuwei
 * @ClassName Triangle.java
 * @description: TODO
 * @date 2023年06月13日
 * @version: 1.0
 */
public class Triangle extends Graphics {

    private Integer a, b, c;

    public Triangle(String name) {
        super(name);
    }

    public Triangle(String name, Integer a, Integer b, Integer c) {
        super(name);
        this.a = a;
        this.b = b;
        this.c = c;
    }

    @Override
    public void draw() {
        if (a.equals(b) && b.equals(c)) {
            System.out.println("画一个等边三角形");
        } else if (a.equals(b) || a.equals(c) || b.equals(c)) {
            System.out.println("画一个等腰边三角形");
        } else if ((a * a) == (b * b + c * c) || b * b == a * a + c * c || c * c == a * a + b * b) {
            System.out.println("画一个直角三角形");
        }else {
            System.out.println("画一个为普通三角形");
        }


    }
}
