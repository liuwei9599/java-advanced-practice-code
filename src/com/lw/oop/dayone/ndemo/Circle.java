package com.lw.oop.dayone.ndemo;

/**
 * @author liuwei
 * @ClassName Circle.java
 * @description: TODO
 * @date 2023年06月13日
 * @version: 1.0
 */
public class Circle extends Graphics{
    private Integer radius;

    public Circle(String name, Integer radius) {
        super(name);
        this.radius = radius;
    }
    @Override
    public void draw() {
        System.out.println("画一个 圆,半径为 "+ radius);
    }
}
