package com.lw.oop.dayone.ndemo;

/**
 * @author liuwei
 * @ClassName GraTest.java
 * @description: TODO
 * @date 2023年06月13日
 * @version: 1.0
 */
public class GraTest {
    public static void main(String[] args) {
        Graphics t= new Triangle("三角形",3,4,5);
        t.draw();
        Graphics t2 = new Square("正方形", 4, 4);
        t2.draw();
        Graphics t3 = new Ellipse("圆形");
        t3.draw();
    }
}
