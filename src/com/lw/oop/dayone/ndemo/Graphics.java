package com.lw.oop.dayone.ndemo;

/**
 * @author liuwei
 * @ClassName Graphics.java
 * @description: TODO
 * @date 2023年06月13日
 * @version: 1.0
 */
public abstract class Graphics {
    String name;

    public Graphics(String name) {
        this.name = name;
    }

    public abstract void draw();

}
