package com.lw.oop.dayone.ldemo.mainpackage;

/**
 * @author liuwei
 * @ClassName MainClass.java
 * @description: TODO
 * @date 2023年06月13日
 * @version: 1.0
 */
public class MainClass {
    private String name;
    private Integer age;
    private String sex;

    public MainClass() {}
    public MainClass(String name, Integer age, String sex) {
        this.name = name;
        this.age = age;
        this.sex = sex;
    }

    public void disPlay(){
        System.out.println("姓名："+this.name + ","+"年龄："+this.age + ","+"性别："+this.sex);
    }

}
