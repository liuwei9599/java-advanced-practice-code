package com.lw.oop.dayone.ldemo;

import com.lw.oop.dayone.ldemo.mainpackage.MianSubClass;

/**
 * @author liuwei
 * @ClassName SamePackage.java
 * @description: TODO
 * @date 2023年06月13日
 * @version: 1.0
 */
public class SamePackage {
    public static void main(String[] args) {
        MianSubClass mys = new MianSubClass();
        mys.disPlay();
    }
}
