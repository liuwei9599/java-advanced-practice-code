package com.lw.oop.dayone.kdemo;

/**
 * @author liuwei
 * @ClassName StaticDemo.java
 * @description: TODO
 * @date 2023年06月13日
 * @version: 1.0
 */
public class StaticDemo {
    private static Integer num1= 10;
    private Integer num2=5;

    public static void main(String[] args) {
        System.out.println(StaticDemo.num1);
        StaticDemo sd = new StaticDemo();
        System.out.println(sd.num2);
    }

}
