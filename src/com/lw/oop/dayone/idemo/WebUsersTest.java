package com.lw.oop.dayone.idemo;

/**
 * @author liuwei
 * @ClassName WebUsersTest.java
 * @description: TODO
 * @date 2023年06月13日
 * @version: 1.0
 */
public class WebUsersTest {
    public static void main(String[] args) {
        WebUsers web1 = new WebUsers("12345", "xaioxiao", "4tokenHeight","abcde@qq.com");
        System.out.println(web1.toString());
        WebUsers web2 = new WebUsers("12345", "xaioxiao", "4tokenHeight");
        System.out.println(web2.toString());
    }
}
