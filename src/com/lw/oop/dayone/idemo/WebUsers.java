package com.lw.oop.dayone.idemo;

/**
 * @author liuwei
 * @ClassName WebUsers.java
 * @description: TODO
 * @date 2023年06月13日
 * @version: 1.0
 */
public class WebUsers {
    private String id;
    private String name;
    private String password;
    private String email;

    public WebUsers() {
    }

    public WebUsers(String id, String name, String password) {
        this.id = id;
        this.name = name;
        this.password = password;
        this.email = id.concat("@qq.com");
    }

    public WebUsers(String id, String name, String password, String email) {
        this.id = id;
        this.name = name;
        this.password = password;
        this.email = email;
    }
    @Override
    public String toString() {
        return "\nID："+id + "\nName："+name + "\nPassword："+password + "\nEmail："+email;
    }
}
