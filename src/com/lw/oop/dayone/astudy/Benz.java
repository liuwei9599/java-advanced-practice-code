package com.lw.oop.dayone.astudy;

/**
 * @author lw
 * @ClassName Benz.java
 * @Description
 * @createTime 2023年06月12日 18:57:00
 */
public class Benz extends Car{
    public Benz(String name) {
        super(name);
    }

    /**
     * 重写run方法
     */
    @Override
    public void run() {
        super.run();
    }
}
