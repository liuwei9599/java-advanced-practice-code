package com.lw.oop.dayone.astudy;

/**
 * @author lw
 * @ClassName InterfaceTest.java
 * @Description
 * @createTime 2023年06月12日 19:07:00
 */
public interface InterfaceTest {
    public static final String name = null;
    void run();
    public abstract void rr();
}
