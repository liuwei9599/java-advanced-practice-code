package com.lw.oop.dayone.astudy;

/**
 * @author lw
 * @ClassName Car.java
 * @Description
 * @createTime 2023年06月12日 18:51:00
 */
public class Car {
    String name;
    public Car(){
        System.out.println("这是Car类无参构造方法！");
    }
    public Car(String name){
        this.name = name;
        System.out.println("这是Car类有参构造方法！");
    }
    public void run(){
        System.out.println(name+"车正在行驶！");
    }

    /**
     *方法重载
     */
    public void shut() {
        System.out.println(name + "车已经停止了！");
    }

    public void shut(String name){
        System.out.println(name + "车已经停止了！");
    }

}
