package com.lw.oop.dayone.astudy;

/**
 * @author lw
 * @ClassName AbstractTest.java
 * @Description
 * @createTime 2023年06月12日 19:04:00
 */
public abstract class AbstractTest {
    String name;
    public abstract void tTest();
    public void run() {
        System.out.println("运行run方法");
    }
}
