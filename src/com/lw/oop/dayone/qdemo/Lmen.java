package com.lw.oop.dayone.qdemo;

/**
 * lmen
 *
 * @author liuwei
 * @ClassName Lmen.java
 * @description: TODO
 * @date 2023年06月13日
 * @version: 1.0
 */
public interface Lmen {
    /**
     * 防盗
     */
    void theftproof();

    /**
     * 防水
     */
    void waterproof();

    /**
     * 防弹
     */
    void bulleproof();
    /**
     * 防火
     */
    void fireproof();
    /**
     * 防锈
     */
    void rustproof();
}
