package com.lw.oop.dayone.qdemo;

/**
 * @author liuwei
 * @ClassName AaDoorTest.java
 * @description: TODO
 * @date 2023年06月13日
 * @version: 1.0
 */
public class AaDoorTest {
    public static void main(String[] args) {
        Door d = new AaDoor();
        d.openDoor();
        d.closeDoor();
        System.out.println("----------------------");
        d.theftproof();
        d.waterproof();
        d.bulleproof();
        d.fireproof();
        d.rustproof();
    }
}
