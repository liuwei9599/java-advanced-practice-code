package com.lw.oop.dayone.qdemo;

/**
 * @author liuwei
 * @ClassName Door.java
 * @description: TODO
 * @date 2023年06月13日
 * @version: 1.0
 */
public abstract class Door implements Lmen{
    /**
     * 开门
     */
    public abstract void openDoor();

    /**
     * 关门
     */
    public abstract void closeDoor();
}
