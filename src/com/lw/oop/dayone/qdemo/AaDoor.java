package com.lw.oop.dayone.qdemo;

/**
 * @author liuwei
 * @ClassName AaDoor.java
 * @description: TODO
 * @date 2023年06月13日
 * @version: 1.0
 */
public class AaDoor extends Door implements Lmen {
    public final static String level1 = "Ⅰ";
    public final static String level2 = "Ⅱ";
    public final static String level3 = "Ⅲ";

    @Override
    public void openDoor() {
        System.out.println("门被打开！");
    }

    @Override
    public void closeDoor() {
        System.out.println("门被关闭！");
    }

    @Override
    public void theftproof() {
        System.out.println("防盗级别" + level2);
    }

    @Override
    public void waterproof() {
        System.out.println("防水级别" + level3);
    }

    @Override
    public void bulleproof() {
        System.out.println("防弹级别" + level1);
    }

    @Override
    public void fireproof() {
        System.out.println("防火级别" + level1);
    }

    @Override
    public void rustproof() {
        System.out.println("防锈级别" + level1);
    }
}
