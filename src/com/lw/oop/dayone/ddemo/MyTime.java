package com.lw.oop.dayone.ddemo;

/**
 * @author liuwei
 * @ClassName MyTime.java
 * @description: TODO
 * @date 2023年06月13日
 * @version: 1.0
 */
public class MyTime {
    private Integer hour;
    private Integer minute;
    private Integer second;

    public MyTime(){

    }
    public MyTime(Integer hour, Integer minute, Integer second) {
        this.hour = hour;
        this.minute = minute;
        this.second = second;
    }
    public Integer addHour(int hou){
        hour = hour+ hou;
        return hour;
    }
    public Integer addMinute(int min) {
        minute = minute + min;
        return minute;
    }
    public Integer addSecond(int sec) {
        second = second + sec;
        return second;
    }
    public Integer subHour(int hou) {
        hour = hour - hou;
        return hour;

    }
    public Integer subMinute(int min) {
        minute = minute - min;
        return minute;
    }

    public Integer subSecond(int sec) {
        second = second - sec;
        return second;
    }




    public void diaPlay(){
        System.out.println("时间为："+hour+"时"+minute+"分"+second+"秒");

    }
}
