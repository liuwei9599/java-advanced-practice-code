package com.lw.oop.dayone.bdemo;

/**
 * @author lw
 * @ClassName GuessNum.java
 * @Description 猜数字
 * @createTime 2023年06月12日 19:17:00
 */
public class GuessNum {
    private Integer num;
    public GuessNum(){
    }
    public GuessNum(Integer num){
        this.num = num;
    }

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }
}
