package com.lw.oop.dayone.bdemo;

import java.util.Scanner;

/**
 * @author lw
 * @ClassName GTest.java
 * @Description
 * @createTime 2023年06月12日 19:20:00
 */
public class GeTest {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        GuessNum g = new GuessNum(100);

        while(true){
            System.out.print(" 请输入一个整数：");
            Integer n;
            if (sc.hasNextInt()) {
                n = sc.nextInt();
                if (g.getNum() > n) {
                    System.out.println("猜小了");
                    continue;
                } else if (g.getNum() < n) {
                    System.out.println("猜大了");
                    continue;
                } else {
                    System.out.println("猜对了");
                    break;
                }
            } else {
                System.out.println("没有输入整数");
                continue;
            }
        }

    }

}
