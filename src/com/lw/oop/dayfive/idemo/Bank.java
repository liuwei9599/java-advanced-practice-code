package com.lw.oop.dayfive.idemo;

/**
 * @author liuwei
 * @ClassName Bank.java
 * @description: TODO
 * @date 2023年06月20日
 * @version: 1.0
 */
public class Bank extends Thread{
    private boolean flag=false;
    private Integer m = 0;
    @Override
    public void run() {
        for (int i = 0; i <3 ; i++) {
            subTick();
        }
    }
    public synchronized void miantick(){
        while(!flag) {
            try {
                //释放锁，进入等待队列
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        m=1000+m;
        System.out.println("储户2存入"+1000+"\t存入后账户金额："+ m);
        //通知一个线程
        notify();
        flag=false;
    }
    public synchronized void subTick(){
        while(flag) {
            try {
                //释放锁，进入等待队列
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        m=1000+m;
        System.out.println("储户1存入"+1000+"\t存入后账户金额："+ m);
        //通知一个线程
        notify();
        flag=true;
    }
    public static void main(String[] args) {
        Bank t1=new Bank();
        t1.start();
        for (int i = 0; i < 3; i++) {
            t1.miantick();
        }

    }
}
