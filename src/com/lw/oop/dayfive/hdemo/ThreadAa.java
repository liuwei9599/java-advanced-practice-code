package com.lw.oop.dayfive.hdemo;

/**
 * @author liuwei
 * @ClassName ThreadAa.java
 * @description: TODO
 * @date 2023年06月20日
 * @version: 1.0
 */
public class ThreadAa implements Runnable{
    private static volatile int printNum = 0;
    private int threadId;

    public ThreadAa(int threadId){
        this.threadId = threadId;
    }
    @Override
    public void run() {
        while(printNum < 75){
            tt();
        }
    }
    public synchronized void tt(){
        if (printNum/5%3 + 1 == threadId){
            for (int i = 0; i <5; i++) {
                System.out.println("线程"+threadId+":"+(++printNum));
            }
            Test.class.notify();
        }else {
            try {
                Test.class.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
