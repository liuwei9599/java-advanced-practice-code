package com.lw.oop.dayfive.hdemo;

/**
 * @author liuwei
 * @ClassName Test.java
 * @description: TODO
 * @date 2023年06月20日
 * @version: 1.0
 */
public class Test implements Runnable {
    private static int i = 1;

    @Override
    public synchronized void run() {
        for (int j = 5; j >= 1; j--) {
            System.out.println(Thread.currentThread().getName() + "---" + i);
            i++;
        }
    }
    public static void main(String[] args) throws InterruptedException {
        Test test = new Test();
        while (i<=75){
            Thread t1 = new Thread(test, "线程1");
            Thread t2 = new Thread(test, "线程2");
            Thread t3 = new Thread(test, "线程3");
            t1.start();
            t1.join();
            t2.start();
            t2.join();
            t3.start();
            t3.join();
        }
    }
}
