package com.lw.oop.dayfive.ademo;

/**
 * @author liuwei
 * @ClassName StrTest.java
 * @description: TODO
 * @date 2023年06月19日
 * @version: 1.0
 */
public class StrTest {
    public static void main(String[] args) {
        String string="Hello,World";
        //在给定的字符串中,搜索指定的字符
        System.out.println(string.indexOf("l"));

        //遍历输出给定字符串中的所有字符
        for (int i = 0; i < string.length(); i++) {
            System.out.print(string.charAt(i)+"\t");
        }
        //给定字符串,包含字母o,将所有的字母o,全部替换为*
        String string1 = string.replaceAll("o", "*");
        System.out.println("\n"+string1);
        String eee = string.replace('e','*');
        System.out.println(eee);
        //给定字符串,长度超过10个字符,截取前5个:subString(起始,结束)
        String substring = string.substring(0, 5);
        System.out.println(substring);
        //给定字符串,长度超过10个字符,截取第3-7个.subString(起始,结束)
        String substring1 = string.substring(2, 7);
        System.out.println(substring1);
        //给定3个字符串,拼接成一个新的字符串
        String s1="HH";
        String s2="MM";
        String s3="SS";
        String ss=s1.concat(s2).concat(s3);
        System.out.println(ss);
        //给定一个字符串,判断是否包含指定的内容
        String str ="I Love You";
        System.out.println(str.contains("You"));

    }
}
