package com.lw.oop.dayfive.ademo;

/**
 * @author liuwei
 * @ClassName StrBufTest.java
 * @description: TODO
 * @date 2023年06月19日
 * @version: 1.0
 */
public class StrBufTest {
    public static void main(String[] args) {
        System.out.println("************StringBuffer****************");
        StringBuffer stringBuffer =new StringBuffer();
        stringBuffer.append("Hello,");
        stringBuffer.append("StringBuffer!");
        System.out.println("stringBuffer的容量："+stringBuffer.capacity());
        stringBuffer.setCharAt(1, 'o');
        System.out.println("修改对象中指定索引的字符："+stringBuffer);
        //移除序列中指定位置的字符
        stringBuffer.deleteCharAt(5);
        System.out.println("移除序列中指定位置的字符的结果："+stringBuffer);
        //用于移除序列中子字符串的字符
        stringBuffer.delete(2,5);
        System.out.println("移除序列中子字符串的字符的结果："+stringBuffer);
        stringBuffer.reverse();
        System.out.println("反转字符串后的结果："+stringBuffer);

        System.out.println("************StringBuilder****************");
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Hello,");
        stringBuilder.append("StringBuilder!");
        System.out.println("stringBuilder的容量："+stringBuilder.capacity());
        stringBuilder.setCharAt(1, 'o');
        System.out.println("修改对象中指定索引的字符："+stringBuilder);
        //移除序列中指定位置的字符
        stringBuilder.deleteCharAt(5);
        System.out.println("移除序列中指定位置的字符的结果："+stringBuilder);
        //用于移除序列中子字符串的字符
        stringBuilder.delete(2,5);
        System.out.println("移除序列中子字符串的字符的结果："+stringBuilder);
        stringBuilder.reverse();
        System.out.println("反转字符串后的结果："+stringBuilder);

    }
}
