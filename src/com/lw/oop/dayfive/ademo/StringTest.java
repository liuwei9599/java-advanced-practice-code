package com.lw.oop.dayfive.ademo;

/**
 * @author liuwei
 * @ClassName String.java
 * @description: TODO
 * @date 2023年06月19日
 * @version: 1.0
 */
public class StringTest {
    public static void main(String[] args) {
        String str=" ababcababsdjkjf hello,world ";
        System.out.println("字符串的长度："+str.length());
        String str1="I love you";
        String str2="HELLO,WORLD";
        String[] split = str1.split(" ");
        for (String s:split) {
            System.out.println("根据指定字符分割字符串："+s);
        }
        System.out.println("截取一个字符串："+str.substring(0, 2));
        System.out.println("字符串的长度："+str.length());
        System.out.println("拼接指定字符串："+str.concat(" world"));
        System.out.println("去除空格："+str.trim());
        System.out.println("把指定的字符串转换为小写："+str2.toLowerCase());
        System.out.println("吧指定字符串转换为大写："+str.toUpperCase());
        System.out.println("替换指定字符串："+str.replaceAll("abc", "HELLO"));
        System.out.println("返回指定元素第一次出现的下标位置："+str.indexOf("d"));
        System.out.println("返回指定字符串最后一次出现的位置："+str.lastIndexOf("d"));
        System.out.println("判断某个字符串是否以指定的字符串结尾："+str.endsWith("world"));
        System.out.println("判断某个字符串是否以指定的字符开始："+str1.startsWith("I"));
        System.out.println("获取某个字符串指定索引的字符："+str1.charAt(0));
        System.out.println("判断指定字符串是否包含某个字符："+str.contains("s"));
        for (char c: str.toCharArray()){
            System.out.println("把指定字符串转换为char数组："+c);
        }
    }
}
