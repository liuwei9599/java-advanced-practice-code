package com.lw.oop.dayfive.edemo;

import java.sql.*;
import java.util.Scanner;

/**
 * @author liuwei
 * @ClassName FruitTest.java
 * @description: TODO
 * @date 2023年06月19日
 * @version: 1.0
 */
public class FruitTest {
    static Statement statement = null;
    static Connection conn;
    static PreparedStatement pstmt = null;
    public void jdbc() throws ClassNotFoundException, SQLException {
        //加载驱动
        Class.forName("com.mysql.jdbc.Driver");
        String url = "jdbc:mysql://localhost:3306/jdbc_test?useUnicode=true&characterEncoding=utf8";
        String user = "root";
        String pas = "root";
        //获取执行者
        conn = DriverManager.getConnection(url, user, pas);
        if (conn != null) {
            System.out.println("连接成功");
            //获取发送 SQL 的对象
            statement = conn.createStatement();
        }else {
            System.out.println("连接失败");
        }
    }
    public String login(String username,String password) throws SQLException {
        if (conn != null) {
            System.out.println("连接成功");
            pstmt = conn.prepareStatement("select * from account where username=? and password=?");
            pstmt.setString(1, username);
            pstmt.setString(2, password);
            ResultSet result = pstmt.executeQuery();
            while (result.next()) {
                String name = result.getString("username");
                System.out.println("登录成功 + 用户名：" + name);
                return name;
            }
        }
        System.out.println("登录失败");
        return null;
    }



    public void update() throws SQLException {
        try{
            if (statement != null) {
                String sql="insert into fruit values('k004','葡萄',2.4,'新疆',100),('k005','樱桃',2.4,'青岛',100)," +
                        "('k006','桃子',2.4,'花果山',100),('k007','香蕉',2.4,'济南',100),('k008','水蜜桃',3.0,'山东',100)";
                //执行SQL语句并接收结果
                int result1 = statement.executeUpdate(sql);
                System.out.println(result1);
                if (result1!=0){
                    System.out.println("插入数据成功！");
                }else {
                    System.out.println("插入数据失败！");
                }
            } else {
                System.out.println("插入数据失败！");
            }
        }catch (SQLException e){
            System.out.println("数据已存在!");
        }
    }

    public void menu(){
        System.out.println("欢迎进入水果商城");
        System.out.println("1.登录");
        System.out.println("2.插入水果数据");
        System.out.println("请输入序号：");
    }

    public void closeAll() throws SQLException {
        if (statement!=null){
            statement.close();
        }
        if (conn!=null){
            conn.close();
        }
        if (pstmt!=null){
            pstmt.close();
        }
    }

    public static void main(String[] args) throws SQLException, ClassNotFoundException {
        Scanner sc=new Scanner(System.in);
        FruitTest f=new FruitTest();
        f.jdbc();
        while (true){
            f.menu();
            int n=sc.nextInt();
            if (n==1){
                System.out.println("请输入用户名：");
                String username=sc.next();
                System.out.println("请输入密码：");
                String password=sc.next();
                String str = f.login(username, password);
                if (str!=null){
                    f.menu();
                    n=sc.nextInt();
                    if (n==1){
                        System.out.println("您已登录！");
                    }else if(n==2){
                        f.update();
                        break;
                    }
                }
            }else if (n==2){
                System.out.println("未登录，请先登录！");
                continue;
            }else {
                System.out.println("输入错误！");
                continue;
            }
        }
        f.closeAll();
    }
}
