package com.lw.oop.dayfive.gdemo;

/**
 * @author liuwei
 * @ClassName TtTest.java
 * @description: TODO
 * @date 2023年06月19日
 * @version: 1.0
 */
public class TtTest extends Thread {
    private boolean flag = false;
    static int i = 1;
    static char c = 'A';

    @Override
    public void run() {
        while (true) {
            if (c <= 'Z') {
                subTick();
            }else {
                break;
            }
        }

    }

    public synchronized void miantick() {
        while (!flag) {
            try {
                //释放锁，进入等待队列
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        if (c <= 'Z') {
            System.out.println(c);
            c++;
        }
        //通知一个线程
        notify();
        flag = false;
    }

    public synchronized void subTick() {
        while (flag) {
            try {
                //释放锁，进入等待队列
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        if (i <= 52) {
            System.out.print(i + "" + (i + 1));
            i = i + 2;
        }
        //通知一个线程
        notify();
        flag = true;
    }

    public static void main(String[] args) {
        TtTest t1 = new TtTest();
        t1.start();
        while(c<='Z'){
            t1.miantick();
        }
    }
}
