package com.lw.oop.dayfive.fdemo;

/**
 * @author liuwei
 * @ClassName TicketTest.java
 * @description: TODO
 * @date 2023年06月19日
 * @version: 1.0
 */
public class TicketTest extends Thread {
    private static Integer num=500;
    @Override
    public void run() {
        while(num>0) {
            tick();
        }
    }
    public synchronized void tick(){
        if(num > 0){
            System.out.println("当前线程"+Thread.currentThread().getName()+"正在卖票"+num--);
        }
    }
    public static void main(String[] args) {
        TicketTest ticketTest = new TicketTest();
        Thread t1 = new Thread(ticketTest , "线程1");
        Thread t2 = new Thread(ticketTest , "线程2");
        Thread t3 = new Thread(ticketTest , "线程3");
        t1.start();
        t2.start();
        t3.start();
    }
}
