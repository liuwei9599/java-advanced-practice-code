package com.lw.oop.dayfive.fdemo;

/**
 * @author liuwei
 * @ClassName ThreadTest.java
 * @description: TODO
 * @date 2023年06月19日
 * @version: 1.0
 */
public class ThreadTest extends Thread{
    private boolean flag=false;

    @Override
    public void run() {
        for (int i = 0; i <3 ; i++) {
            subTick();
        }
    }
    public synchronized void miantick(){
        while(!flag) {
            try {
                //释放锁，进入等待队列
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        //主线程输出5次
        for(int i=0;i<5;i++) {
            System.out.println("主线程输出第 "+(i+1) +" 次");
        }
        //通知一个线程
        notify();
        flag=false;
    }
    public synchronized void subTick(){
        while(flag) {
            try {
                //释放锁，进入等待队列
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        //子线程输出3次
        for(int i=0;i<3;i++) {
            System.out.println("子线程输出第 "+(i+1) +" 次");
        }
        //通知一个线程
        notify();
        flag=true;
    }
    public static void main(String[] args) {
        ThreadTest t1=new ThreadTest();
        t1.start();
        for (int i = 0; i < 3; i++) {
            t1.miantick();
        }
    }
}
