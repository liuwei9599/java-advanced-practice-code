package com.lw.oop.petshopping;

/**
 * @author liuwei
 * @ClassName test.java
 * @description: TODO
 * @date 2023年06月21日
 * @version: 1.0
 */
public class Test {
    public void add(int i) throws NullPointerException {
        if (i==0) {
            throw new NullPointerException();
        }
        System.out.println("add异常");
    }
    public static void main(String[] args) {
        Test test = new Test();
        try {
            test.add(0);
            System.out.println("方法返回");
        } catch (Exception e) {
            System.out.println("捕获异常");;
        }
    }
}
