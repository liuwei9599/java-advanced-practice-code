/*
 Navicat Premium Data Transfer

 Source Server         : php_localhost
 Source Server Type    : MySQL
 Source Server Version : 50726 (5.7.26)
 Source Host           : localhost:3306
 Source Schema         : jdbc_test

 Target Server Type    : MySQL
 Target Server Version : 50726 (5.7.26)
 File Encoding         : 65001

 Date: 30/07/2023 22:46:52
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for account
-- ----------------------------
DROP TABLE IF EXISTS `account`;
CREATE TABLE `account`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of account
-- ----------------------------
INSERT INTO `account` VALUES (1, 'zhangsan', '123');
INSERT INTO `account` VALUES (2, 'lisi', '456');

-- ----------------------------
-- Table structure for employees
-- ----------------------------
DROP TABLE IF EXISTS `employees`;
CREATE TABLE `employees`  (
  `empId` int(11) NOT NULL AUTO_INCREMENT,
  `empName` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `job` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `topId` int(11) NULL DEFAULT NULL,
  `hdate` date NULL DEFAULT NULL,
  `sal` int(11) NULL DEFAULT NULL,
  `bonus` int(11) NULL DEFAULT NULL,
  `deptId` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`empId`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 51 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of employees
-- ----------------------------
INSERT INTO `employees` VALUES (4, '小黄', '前端工程师', 1, '2023-05-25', 30, 2000, 1);
INSERT INTO `employees` VALUES (5, '小青', '运维工程师', 1, '2023-05-25', 30, 2000, 1);
INSERT INTO `employees` VALUES (50, '小蓝', '人事', 1, '2023-05-25', 30, 8000, 1);
INSERT INTO `employees` VALUES (7, 'qwatt', 'Java开发', 1, '2023-06-20', 30, 4000, 1);

-- ----------------------------
-- Table structure for fruit
-- ----------------------------
DROP TABLE IF EXISTS `fruit`;
CREATE TABLE `fruit`  (
  `goods_no` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `goods_name` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `price` decimal(5, 2) NULL DEFAULT NULL,
  `place` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `remain` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`goods_no`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of fruit
-- ----------------------------
INSERT INTO `fruit` VALUES ('k001', '苹果', 2.40, '烟台', 100);
INSERT INTO `fruit` VALUES ('k002', '菠萝', 1.40, '广东', 100);
INSERT INTO `fruit` VALUES ('k003', '桔子', 2.40, '福州', 100);
INSERT INTO `fruit` VALUES ('k004', '葡萄', 2.40, '新疆', 100);
INSERT INTO `fruit` VALUES ('k005', '樱桃', 2.40, '青岛', 100);
INSERT INTO `fruit` VALUES ('k006', '桃子', 2.40, '花果山', 100);
INSERT INTO `fruit` VALUES ('k007', '香蕉', 2.40, '济南', 100);
INSERT INTO `fruit` VALUES ('k008', '水蜜桃', 3.00, '山东', 100);

-- ----------------------------
-- Table structure for student
-- ----------------------------
DROP TABLE IF EXISTS `student`;
CREATE TABLE `student`  (
  `id` int(11) NOT NULL COMMENT '学号',
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '姓名',
  `sex` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '性别',
  `age` int(11) NULL DEFAULT NULL COMMENT '年龄',
  `tel` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '电话',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of student
-- ----------------------------
INSERT INTO `student` VALUES (2023001, '孙悟空001', '男', 20, '17823454357');
INSERT INTO `student` VALUES (2023003, '猪八戒', '男', 20, '13534567855');

-- ----------------------------
-- Table structure for studentinfo
-- ----------------------------
DROP TABLE IF EXISTS `studentinfo`;
CREATE TABLE `studentinfo`  (
  `StudentNo` int(11) NOT NULL,
  `LoginPwd` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `StudentName` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `Sex` bit(1) NOT NULL,
  `GradeId` int(11) NOT NULL,
  `Phone` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `BornDate` datetime NOT NULL,
  `Address` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '地址不详',
  `Email` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `IDENTITYCard` varchar(18) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `age` int(11) NULL DEFAULT NULL,
  `subjects` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`StudentNo`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of studentinfo
-- ----------------------------
INSERT INTO `studentinfo` VALUES (221, '123', 'xx', b'1', 1, '15334567890', '2023-05-29 17:40:37', '北京', '2412349933', '141122200012121234', 27, NULL);
INSERT INTO `studentinfo` VALUES (223, '123', 'xx', b'1', 1, '15334567890', '2023-05-29 17:40:37', '北京', '2412349933', '141122200012121234', 25, NULL);
INSERT INTO `studentinfo` VALUES (222, '123', 'yy', b'0', 1, '15334567890', '2023-05-29 17:40:37', '地址不详', '2412349933', '141122200012021234', 25, NULL);

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户名',
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '密码',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (1, 'admin', 'admin');

SET FOREIGN_KEY_CHECKS = 1;
